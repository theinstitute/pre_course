# Mastering the Command Line 


Learning how to navigate the command line is an essential skill for any developer. Use the challenges and resources outlined in this section to build your confidence and understanding of the command-line. 

Tasks: 

1. [The command line murder mysteries](https://github.com/veltman/clmystery)

### Checklist:

Before moving on, ensure you understand and can use the following commands: 

- [ ] date 
- [ ] ls
- [ ] pwd
- [ ] cd
- [ ] touch
- [ ] mkdir
- [ ] rmdir
- [ ] rm
- [ ] cp
- [ ] mv
- [ ] cat
- [ ] less
- [ ] head
- [ ] tail
- [ ] man
- [ ] find
- [ ] grep
- [ ] wc
- [ ] whoami
- [ ] chmod
- [ ] sudo
- [ ] echo
- [ ] export
- [ ] ps
- [ ] env
- [ ] ruby
- [ ] pgrep