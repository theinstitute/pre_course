# Stride etiquette


Many of us will be quite new to using Stride as a community platform. There hasn't been any established, well-understood or written norms of how a community should use such platforms to communicate and connect with one another.

Therefore, to make the experience in the community more pleasant for all members, let's have a common understanding on some things. This is Version 0.1 and we will continue to add iterate on this as we learn about what works best for our community. 

**Language**

Written communications can easily be misintepreted. Let's be mindful of how our words and tone might be understood, especially by members from different cultures and backgrounds. Also, it is usually best to avoid the use of sarcasm in written communication as it rarely communicates well.

**Using Stride to ask for help**

If any one of us need help, feel free to ask in the appropriate Stride channels. Someone will try and help by answering us or by pointing us in the direction of someone who could offer more insight.
It is important not to take it personally if you do not receive an immediate response to your request - many of us are either at work or learning to code along with you, so can't commit to being on Stride at all times. If you do not receive a response, feel free to ask the question again at a different time of the day when people may be more readily available to assist.

**Notifications**

@here pings those who are online in the channel. Let's use it only when we need help with something and/or want to get the immediate attention of those online.
@all pings those who are in the channel and sends them an email. Let's use it responsibly and sparingly.
