# Course Outline

### Term 1

**Stage 1: Core skills**

_15 week intensive training combining independent study, e-learning, peer-to-peer and lecture-based teaching with practical project based learning._
_Designed and delivered by THG faculty._

Programming Fundamentals in Python

Operating Systems

Computer Systems Architecture

Databases
 
### Term 2

**Stage 2a: Advanced Skills**

_11 week intensive training combining independent study, e-learning, peer-to-peer and lecture-based teaching with an emphasis on practical project based learning._
_Designed and delivered by THG faculty._

Computational Thinking and Advanced Python

Object Oriented Programming in Java

Web Development (including Networks and Security)
 
**Stage 2b: Practical Application**

-        Project work
-        Solving real business problems
-        In small groups alongside THG technologists and with support from faculty



-----------------------

### Industry Primer

_Runs concurrently with the Core Skills stage in Term 1 and taught through a combination of lectures and workshops delivered and facilitated by internal faculty and industry speakers._

-        eCommerce
-        Marketing
-        Life of an Order
-        Life of a Customer
-        Life of a Product
-        Customer Insight
-        Global Considerations
-        Financial Considerations


### Professional Skills

_Taught via workshops and industry speakers throughout the 6 month course. This aspect of the course may vary depending on the availability of our industry speakers and facilitators_

**Social issues:**
-        Social Implications of Computing
-        Information, Privacy and Security

**Professional Issues:**
-        Software Engineering and the Law
-        Ethics in Software Engineering

**Skills:**
-        Presentation and Public Speaking Skills
-        Collaborative Working
-        Time Management
-        Developing a Growth Mindset
 
 
        

