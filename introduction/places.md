# Developer-friendly places to work 

- Media City
    - [Grindsmith](http://www.grindsmith.com/media-city/)
    - [Pokusevski's](https://pokusevskis.com/)
    - [Ziferblat](http://www.ziferblat.co.uk/media-city.html)

- Manchester
    - [Takk](http://takkmcr.com/)
    - [Foundation](http://www.foundationcoffeehouse.co.uk/)
    - [Chapter One](https://www.chapteronebooks.co.uk)
    - [Ziferblat](http://www.ziferblat.co.uk)
    - [Ezra & Gil](http://www.ezraandgil.com)