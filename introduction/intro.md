# Getting started

We've compiled some useful resources and information for you. The links below are designed to help you prepare for your time at THG Institute and for what (for many of you) will be a new and different approach to learning.

 
-  [Mindset](mindset.md)
 
-  [Effective learning](effective_learning.md)

-  [Managing your time](time_management.md)

-  [Course outline](outline.md)
 
-  [How to ask for help (our escalation process)](help.md)

-  [Why you should document your learning and consolidate your knowledge with a blog](blog.md)
 
-  [Places to study in and around Manchester and Media City](places.md)


-------------------------------------


#### Additional Resources

 [Learning to code when it gets dark](https://medium.freecodecamp.org/learning-to-code-when-it-gets-dark-e485edfb58fd)

 [The 'Do Something' Principle](https://markmanson.net/do-something)
 
 [Why learning to code can be so hard](https://www.thinkful.com/blog/why-learning-to-code-is-so-damn-hard)
 
 [Improve your typing speed with Typeracer](https://play.typeracer.com/)


 
 