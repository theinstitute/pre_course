# Blog to learn

_Why and how you should keep a blog of your learning._

Whenever you are learning something new, it's a good idea to keep track of your learning process. One way to accomplish this is to share your experience and write a blog.  Reflecting on your thoughts and the progress you’re making will help you stay more structured and organised. That, in turn, will clear your mind and help you focus. Reflecting on your thoughts and the progress you’re making will help you stay more structured and organised. That, in turn, will clear your mind and help you focus.

Even as a beginner, sharing your thoughts through a blog will help not only yourself but others as well. You don’t need to be an expert in the field. Just be one step ahead of another beginner and you will be able to teach them. 

### Benefits from blogging about your progress

Blogging and/or keeping a journal of your learning brings about structure, perspective and focus. 

**Learn by teaching**

_"If you can’t explain it simply, you don’t understand it well enough."_ - Albert Einstein

Writing about programming and other technical topics offers an amazing opportunity to learn and improve your skills. It requires you to have done the appropriate research, to have written clean code examples and to ensure your writing is easy to understand. If you attempt to reduce your learning into a concise and coherent blog post, it ensures that you interrogate and consolidate your own understanding of the topic: your own learning is enhanced through the process of writing for an audience. 

**Learn by sharing**

After having finished writing a blog post, you will have a much better understanding about the subject. If you share the post, you will also receive invaluable feedback from individuals who have more experience than you. 
 

Put simply, there are a few key benefits that you will receive from blogging whilst learning to code: 

- You get the opportunity to reflect on what you are learning
- You are creating a timeline of your learning
- You are able to consolidate your understanding of what you are learning
- You will receive direct feedback 
- You will be able to help others who are going through the same process. 


### Additional reading

[How and why to keep a technical blog](http://blog.flatironschool.com/the-benefits-of-blogging-how-and-why-to-keep-a-technical-blog/) - Flatiron School 





