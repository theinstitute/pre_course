# Time Management

You will have more success with your independent learning by putting consistent time into it rather than working on it every other day. Building a habit of studying everyday at a specific time and for a specific amount of time will ensure you make consistent progress.

## How To Avoid Procrastination

Procrastination has the potential to present one of the biggest obstacles to your progression. There are some ways you can avoid this being the case: 


#### Use The Pomodoro Technique:

The Pomodoro Technique is a way of managing your time in order to stay focused. Essentially, you set a timer for 25 minutes and work on a task until the timer goes off. You then take a 5 minute break and go again for 25 minutes once your break is over.

If you get distracted or interrupted during the 25 minutes you have to start the 25 minutes of work over again.

The Pomodoro Technique is great for avoiding procrastination as it forces you to work without distractions. And since it’s only for 25 minutes before taking a break, it’s not overwhelming, making it harder to rationalise procrastinating. Working in this way can also easily be implemented whether working individually, in pairs, or in larger groups. 

To better understand how harnessing the Pomodor Technique can improve your productivity, take a read of [this](https://medium.com/life-hacks/more-productivity-with-the-pomodoro-technique-d7ce8926ec0c) article.

You can take your [pick of pomodor apps to use](https://zapier.com/blog/best-pomodoro-apps/).

#### Avoid Digital Distractions:

Digital distractions are email and other push notifications, or 'time-wasting websites' like social networks. These kinds of distractions break your focus and make it tempting to procrastinate; they should be avoided during study time.

**Solution:** turn off notifications and add a blocker to your internet to limit time on time-wasting sites like Facebook.

#### Avoid Physical Distractions In Your Environment:

Physical distractions are distractions from your environment, things like a TV in the background, other people speaking etc. These are just as much a nuisance to breaking your focus as digital distractions.

**Solution:** find a quiet study place in your home you can go to to focus. If that’s not an option you can use noise cancelling head phones to block out any noise distractions from your environment.