# Community 
_Get involved with the local tech community._

There are a number of ways in which you can participate in the Manchester tech community. 

Below is a list of organisations that are active. It is by no means exhaustive. Other lists that already exist include the [technw events calendar](http://technw.uk/calendar), Manchester Digital's [events calendar](https://www.manchesterdigital.com/events), Madlab's [upcoming events](https://madlab.org.uk/events/), Federations's ['presents' series](http://www.thefederation.coop/whats-on.html). You should also keep an eye on relevant events on [eventbrite](https://www.eventbrite.co.uk/) and [meetup](https://www.meetup.com/)


### Tech events

### Socials/Networking

### Outreach and volunteering opportunities

### Conferences (Manchester)

UpfrontConf: _A front-end connference for anyone who makes for the web_: https://upfrontconf.com/
https://twitter.com/upfrontconf

BarCamp: https://www.barcampmanchester.co.uk/
https://twitter.com/barcampmcr




