# Asking for help: our escalation process (or what to do when you get stuck)

You will inevitably be stuck at some point, it may be due to a concept that you are having difficulty understanding or it may be due to something not working when you are doing project work. Whatever it is, use the following processes to become unstuck:


### Step 1: Identify and isolate keywords

1. Try to identify some specific words that provide some context to your problem. 'It's not working' is not descriptive enough. 

2. *Isolate Keywords.* Gather a small group of keywords that describe the problem area, like `Array`, `merge conflict`, or `Terminal`.

3. *Isolate Error Keywords.* If you have an error message, identify and add these keywords to your results.

### Step 2: Google it!

Using your keywords from the above process, do some research to identify the issue (and a possible solution).

You're going to become an expert in googling things. 90% of the time, you'll find a solution to your problem somewhere in the internet. It's likely that someone else will have experienced the same question, problem or error that you have, and that the issue and/or the solution will be documented for you somewhere on the internet. 

### Step 3: Stride

If after conducting your own research into the problem you are unable to find a solution (or if you are unsure whether you have found the appropriate one), then it is time to ask the rest of your Institute cohort for some help. It may be that as you are working through the same content, some of them will have experienced the same issue or similar. When turning to Stride for help from your cohort, it is important to structure your question/issue clearly so that people can understand and assist: 

A good technical query would answer the following three questions: 

1. What is the issue (_remember to use the keywords you've identified_)
2. Why did it happen? (_what were you working on and what changes had you made prior to the issue/error occurring_)
3. What have you already tried? (_when you researched the issue, you may have found and attempted to implement some of the suggested solutions_)

**REMEMBER: show the *code* and *relevant output* (error messages, log, display, screenshots, etc.)**


### Step 4: Reach out to a member of THG Institute faculty. 

If all else fails, and you are satisfied you have attempted all of the above, reach out to a faculty member via direct message on Stride. But a word of warning, don't be disappointed if the answer comes as more questions. Where possible, we will always want you to find your own way to a solution. 