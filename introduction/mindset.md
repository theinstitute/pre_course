# Mindset 

Your mindset is very important when developing any new skill.  Your mindset will have more of an impact on your chances of success than just about anything else.

There is a wide body of research that supports that intelligence is not fixed, that it can be developed. According to [Carol Dweck](https://www.ted.com/talks/carol_dweck_the_power_of_believing_that_you_can_improve), people usually fall into one of the following two mindsets:

**Fixed Mindset:**
_Someone who believes if they don’t get something on their first attempt they never will and that they aren’t smart enough to be able to do or understand some things_.

**Growth Mindset:**
_Someone that believes they can get better at anything with effort and persistence._


In 2014, Dweck published findings based on two decades of research on growth versus fixed mindsets amongst individuals and organizations. Her research found that those who believe their talents can be developed over time have a growth mindset while those who believe their talents are innate gifts and therefore stable across time have a fixed mindset. Individuals with a growth mindset tend to achieve more than those with a more fixed mindset.

Developing a growth mindset includes 4 key components: setting achievable goals, encouraging hard work, reinforcing good strategies, and receiving input from others.

Not only can the process of learning to code be used to help develop a growth mindset, there are aspects of coding that help reinforce this mindset and the curriculum and teaching methodology at THG Institute has been designed to reinforce this:


**Achievable Goals**


_“Learning how to program didn’t start off with wanting to learn all of computer science or trying to master this discipline or anything like that. It started off because I wanted to do this one simple thing – I wanted to make something that was fun for me and my sisters.”_ –Mark Zuckerberg, CEO Facebook


One of the most popular first exercises when learning to code is how to display “Hello World” on the screen. This simple exercise is designed to provide students with an initial understanding of how to give the computer an instruction and get immediate feedback on whether or not it worked. If it doesn’t, the code is simple enough to go back and fix until you get it right. If it works, there is immediate gratification – a true sense of accomplishment.

Similarly, as people begin to learn more and more about programming, the complexity of the projects typically increases. However the process of development, compiling, and execution remains similar. And, once executed, the developer quickly learns whether or not their code works as intended. This consistent, incremental progress over time builds confidence of kids in the ability to build using technology.


**Hard Work**


_“Learning to write programs stretches your mind, and helps you think better, creates a way of thinking about things that I think is helpful in all domains.”_ – Bill Gates, Chairman Microsoft


As a 13 year old, Bill Gates began to show an interest in computer programming. He spent much of his free time working on the terminal and was fascinated with what a computer could do. When his time on the terminal became limited, he exploited a technical glitch to gain more time to learn how to program.

As novice programmers begin to learn more about technology systems, they begin to develop a curiosity about what else they can add to their projects or begin to imagine what they might build from the ground up.  This progression from a fundamental understanding of how the systems work to leveraging the system to accomplish additional goals is essential. This illustrates that there is always more to learn and that to truly become better at coding requires work to implement increasingly complex functionality.


**Develop & Reinforce Good Strategies**


_“Programming allows you to think about thinking, and while debugging you learn learning.”_ – Nicholas Negroponte, Founder & Chairman Emeritus of MIT’s Media Lab


Debugging is the process of finding and resolving of defects that prevent correct operation of computer software or a system. Debugging tends to be harder when various subsystems are tightly coupled, as changes in one may cause bugs to emerge in another.

As novice programmers build their own projects using hardware or software, they quickly learn that blocks/lines of code don’t always work as intended. This enables them to develop the skill of going back through to review the code to see where they went wrong. Often during this time, our teaching faculty will encourage you to struggle to find your errors so that you can build your familiarity with reviewing the code blocks/lines to see which parts are correct and then identifying potential sources of errors. The process of breaking down a problem into smaller chunks, validating functionality and then rebuilding the full program to achieve a desired result is an essential element of learning to code.

Since bugs are such a familiar and expected part of technology systems, people who are learning to code should come to view the identification and resolution as part of the building process versus any inherent shortcoming in their talents. Our instructors help reinforce this key insight by encouraging the problem solving process versus highlighting any mistakes made during development.


**Input from Others**


_“Mathematicians stand on each others’ shoulders and computer scientists stand on each others’ toes.”_ – Richard Hamming, Mathematician


Open Source software is computer software with its source code made available with a license in which the copyright holder provides the rights to study, change and distribute the software to anyone and for any purpose. Oftentimes, open source software is developed in a collaborative manner.

One of the best elements of modern day programmers is the development of a community that encourages sharing their completed work with others. Not only does this help reinforce their learning but it also teaches them the importance of cooperating with others. By sharing their projects developers quickly realize that they are not competing with their peers but rather collaboratively learning from each other to ultimately build better projects.


**What does this mean for you?** 

It means you can learn new skills and develop new talents with persistence and determination. There will be many times throughout your career as a developer that you will be stuck with a concept or a programming problem and may find yourself questioning your ability.

When you find yourself in this position, remind yourself that you may not get it yet but with persistence and grit you will. Struggling with something is growth. It doesn’t matter how long you struggle with a concept or project; all that matters is that you have the grit and tenacity to see it through, that’s how real learning happens.

While you’re working through the curriculum, embrace the struggles you encounter with learning difficult concepts and completing projects. And celebrate your persistence at overcoming those struggles.

When you find yourself questioning your ability, reflect on the successes you have already had while learning to program - the projects you have completed and the concepts you, at one time, didn’t understand but now do. This is all the proof you need that you can do it.


_To learn more about the growth mindset use these resources:_



- [The power of believeing you can improve](https://www.ted.com/talks/carol_dweck_the_power_of_believing_that_you_can_improve) - Carol Dweck

- [Grit: The power of passion and perserverence](https://www.ted.com/talks/angela_lee_duckworth_grit_the_power_of_passion_and_perseverance) - Angela Lee Duckworth

- [The learning myth: Why I'll never tell my son he's smart](https://www.khanacademy.org/talks-and-interviews/conversations-with-sal/a/the-learning-myth-why-ill-never-tell-my-son-hes-smart) - Sal Khan










