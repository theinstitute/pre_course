# Effective Learning

Broadly speaking, our curriculum can be broken down into two fundamental learning components: **lessons** and **projects**. 

**Lessons** will usually take the form of a lecture, a workshop, or a group exercise and will often be accompanied by supplemtary resources for you to read and watch. These are intended to introduce you to concepts and theory. 
Most lessons will be followed by **projects**. These are hands-on and practical in nature and designed to allow you to practice and solidify the concepts you have learned about in your previous lessons.

Effective learning requires context. It is through learning concepts and then applying them that you will be able to understand how things works. 

When learning, your mind will switch between the following two states: 

**Focus mode:**
This state occurs when you are consciously focusing on learning, reading, watching videos, or working on a project.

**Diffuse mode:**
This state occurs subconsciously, at times when you are not actively learning, such as doing the dishes, exercising, sleeping, etc. When in this state your mind goes about the business of connecting what you have been learning to the other things you know. This is where break-throughs happen.

It’s important to know that your mind goes through these two states to learn because you can utilise this to make your learning more efficient. When stuck on a concept or project taking a break to refresh and let your subconscious go to work connecting what you are learning together more often than not leads to a solution to your problem. The trick is that you have to put effort into solving the problem first and then take a break.


An effective learning model, and one which we will encourage you to adhere to with us, is to: 

1. Understand it

_Learn the theory and the concepts._, 

2. Practice it 

_Apply the knowledge you have acquired_, 

3. Teach it 

_Pass on your knowledge to others_

**Learning by teaching:**

The process of teaching others is extremely valuable in validating and consolidating your own knowledge. You can practice the method of learning by teaching in a number of ways: 

- Take advantage of opportunities to share your knowledge with others in our community. 

_This can be informally by helping coach others through any issues they are having, or we would also encourage you to take the lead and deliver talks and workshops on topics of your choosing. Throughout the course, there will be plenty of opportunities for you to get involved in this way._

- Volunteer for a community group or school outreach project and help teach others who are learning programming. 

_There are a number of initiatives you can get involved with and you can find out more about them_ [here]()

- Recording your own learning and projects through a blog. 

**A blog is a very powerful learning tool and a fantastic platform for any developer.** _Find out more about why you should blog about your learning_ [here]()



### Additional Resources


To learn more about the best ways to learn, [Learning how to Learn](https://www.coursera.org/learn/learning-how-to-learn) on Coursera is highly recommended.



[Back to contents](./README.md)     |        [Next]()

