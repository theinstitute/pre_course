# THG Technology and Innovation Accelerator 

**Welcome to The Hut Group and to THG Technology and Innovation Accelerator**



### Pre-course 

Prior to starting with us in September, you may wish to familiarise yourself with some of the topics below. These materials will help you to prepare for your time at THG Institute of Technology and Innovation and for your future career as a software developer. 

_These are by no means pre-requisites and many of the topics will be revisited when you arrive at THG Institute._

### Contents

[Getting Started](./introduction/intro.md)
 - [Mindset](./introduction/mindset.md)
 - [Effective Learning](./introduction/effective_learning.md)
 - [Managing Your Time](./introduction/time_management.md)
 - [Course Outline](./introduction/outline.md)
 - [Escalation Process](./introduction/help.md)
 - [Places to study in and around Manchester](./introduction/places.md)

[Developer Tools](./dev_tools/dev_tools.md)
 - [The Command Line](./dev_tools/command_line.md)
 - [Git and Version Control](./dev_tools/git.md)

[Python](./python/python.md)

### Communication

You should have received an invite to join our dedicated [Stride](https://www.stride.com/) workspace - please make sure you're signed up to it. Use it to communicate with us, it's so much better than email! We'll be sending resources, reminders and announcements via Stride so we recommend that you download the desktop app and keep it open so that it's easily available. Please do make use of the workspace and introduce yourself to the rest of your cohort.

Stride is a team communication and collaboration tool created by [Atlassian](https://www.atlassian.com/). Our THG technology teams currently use Hipchat (Atlassian's alternative communication tool). Once you start at THG and are provided with your official THG email addresses, you will receive an invitation to our wider THG communication platform. 

Important reading:

[THG Accelerator Stride etiquette guide](stride.md)


### Useful Resources

- [A list of places to study in and around Manchester](introduction/places.md)
- [THG Website](https://www.thg.com/)
- [THG Institute of Technology and Innovation Website]() (launching in August)






