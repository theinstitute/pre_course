# Python

[Code Academy:](https://www.codecademy.com/learn/learn-python) _A rudimentary introduction to programming in Python._ 

[Udacity:](https://eu.udacity.com/course/programming-foundations-with-python--ud036) _Programming Foundations in Python._

[A Byte of Python:](https://python.swaroopch.com/) _An online beginner’s guide to programming in Python_

[Codewars:](https://www.codewars.com/) _Sign up to codewars and  have a go at completing 8 and 7 kyu katas for Python._


Books:

[How To Think Like a Computer Scientist](http://openbookproject.net/thinkcs/python/english2e/)

[Introduction to Computation and Programming](https://doc.lagout.org/programmation/python/Introduction%20to%20Computation%20and%20Programming%20using%20Python%20%28rev.%20ed.%29%20%5BGuttag%202013-08-09%5D.pdf)

Interpretation of computer programs ([online portions here](http://www-inst.eecs.berkeley.edu/~cs61a/sp12/book/))
 
